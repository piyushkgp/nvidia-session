
### Notes
###### How do we do Quantization?
Consider we have a network in FP32.
Create a reference distribution of activations in the FP32 network.

We select a threshold T and map mod(activation) higher than T to + or - 128.
Rest, we convert to 8-bit signed INT.

With different thresholds:
- Create histogram of activations
- convert into a probability distribution.
- calculate KL divergence of this distribution with with the reference distribution of weights
in the FP32 network.


#### TensorRT background for optimized inference engine
- Precision calibration
- layer fusion/removal - 1x1 conv are removed, relus are merged
- kernel auto tuning

Notice that in classification networks, accuracy will decrease but in object detection algorithms, it wont affect because we are selecting objects which cross a threshold and our objects will still be able to cross the threshold.


##### LAB
```
docker ps -a

docker cp lab/lab/lab1 <CONT_ID>:/workspace/

nvidia-docker run -it --runtime=nvidia -p 5008:8888 nvcr.io/nvidia/tensorrt:18.08-py3

bash /opt/tensorrt/python/python_setup.sh && wget developer.download.nvidia.com/devblogs/tftrt_sample.tar.xz && tar xvxf tftrt_sample.tar.xz
```

Works!!
```
nvidia-docker run -it -p 5002:8888 --name piyushS deepshi/trtlab
```



##### References:
<https://docs.nvidia.com/deeplearning/sdk/tensorrt-developer-guide/index.html>


<!-- cred:
~/.dcoker/config.json

sukritrao

{
	"auths": {
		"https://index.docker.io/v1/": {
			"auth": "c3Vrcml0cmFvOlN1a1JpVFJhb0A5Ng=="
		}
	},
	"HttpHeaders": {
		"User-Agent": "Docker-Client/18.09.1 (linux)"
	}
} -->
