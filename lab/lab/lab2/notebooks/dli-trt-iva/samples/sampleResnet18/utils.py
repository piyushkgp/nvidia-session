from PIL import Image
import numpy as np
import itertools
import cv2

def prepare_data(image_path, dst_h, dst_w):
    ppm_image = Image.open(image_path)
    # Resize image
    size = (dst_w, dst_h)
    img = ppm_image.resize(size, Image.NEAREST)
    # RGB to BGR
    r, g, b = img.split()
    img = np.asarray([np.array(b), np.array(g), np.array(r)])
    # convert image to float
    img = img.astype(np.float32)
    # scale the pixel by 255
    img = img / 255.0
    return img

def parse_result(engine, conv_res, bbox_res):

    cls_num = engine.output_dim[0].C()
    grid_x = engine.output_dim[0].W()
    grid_y = engine.output_dim[0].H()
    gridsize = grid_x * grid_y;

    target_shape = [grid_x, grid_y]
    input_dim = [engine.input_dim[0].W(), engine.input_dim[0].H()]
    bbox_norm = [engine.input_dim[0].W(), engine.input_dim[0].H()]

    gc_centers_0 = [float((i * 16 + 0.5)/bbox_norm[0]) for i in range(target_shape[0])]
    gc_centers_1 = [float((i * 16 + 0.5)/bbox_norm[1]) for i in range(target_shape[1])]

    det_list = []
    for c in range(cls_num):
        rect_list = []
        for h, w in itertools.product(range(grid_y), range(grid_x)): 
            if conv_res[c][h][w] >= 0.2:
                rectx1 = int((bbox_res[c * 4    ][h][w] - gc_centers_0[w]) * (-bbox_norm[0]))
                recty1 = int((bbox_res[c * 4 + 1][h][w] - gc_centers_1[h]) * (-bbox_norm[1]))
                rectx2 = int((bbox_res[c * 4 + 2][h][w] + gc_centers_0[w]) * (bbox_norm[0]))
                recty2 = int((bbox_res[c * 4 + 3][h][w] + gc_centers_1[h]) * (bbox_norm[1]))

                rectx1 = 0 if rectx1 < 0 else ((input_dim[0] - 1) if rectx1 > input_dim[0] else rectx1) 
                recty1 = 0 if recty1 < 0 else ((input_dim[1] - 1) if recty1 > input_dim[1] else recty1) 
                rectx2 = 0 if rectx2 < 0 else ((input_dim[0] - 1) if rectx2 > input_dim[0] else rectx2) 
                recty2 = 0 if recty2 < 0 else ((input_dim[1] - 1) if recty2 > input_dim[1] else recty2) 
                rect_list.append([rectx1, recty1, rectx2 - rectx1, recty2 - recty1])
        det_obj, weights = cv2.groupRectangles(rect_list, 1, 0.2)
        if len(det_obj) > 0:
            det_list.append(det_obj)

    return det_list
