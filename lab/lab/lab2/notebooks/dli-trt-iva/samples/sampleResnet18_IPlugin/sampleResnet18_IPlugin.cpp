#include <assert.h>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <cuda_runtime_api.h>
#include <cudnn.h>
#include <memory>

#include "NvInfer.h"
#include "NvCaffeParser.h"

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>

using namespace nvinfer1;
using namespace nvcaffeparser1;

#define CHECK(status)                                   \
{                                                       \
    if (status != 0)                                    \
    {                                                   \
        std::cout << "Cuda failure: " << status;        \
        abort();                                        \
    }                                                   \
}

const char* DEPLOY_FILE = "../../model/resnet18/resnet18.prototxt";
const char* MODEL_FILE = "../../model/resnet18/resnet18.caffemodel";
const char* INPUT_BLOB_NAME = "data";
const char* OUTPUT_BLOB_CONV_NAME = "Layer11_cov";
const char* OUTPUT_BLOB_BBOX_NAME = "Layer11_bbox";
static const int BATCH_SIZE = 4;
static const int INPUT_C = 3;
static const int INPUT_H = 368;
static const int INPUT_W = 640;
static const int CLASS_NUM = 3;
static const std::string CLASS_NAME[CLASS_NUM] = {"Person", "TwoWheelers", "car"};
static const float CLASS_THRESHOLD[CLASS_NUM] = {0.2f, 0.2f, 0.2f};
static const int BBOX_MAX_NUM = 32;
static std::vector<std::string> IMAGE_LIST = { "01.ppm", "02.ppm", "03.ppm", "04.ppm" };
static const float SCALER = 0.00392156f; // 1.0/255
static const int TIMING_ITERATIONS = 1;

// Logger for TensorRT info/warning/errors
class Logger : public nvinfer1::ILogger
{
    public:
        void log(nvinfer1::ILogger::Severity severity, const char* msg) override
        {
            // suppress info-level messages
            if (severity == Severity::kINFO) return;

            switch (severity)
            {
                case Severity::kINTERNAL_ERROR: std::cerr << "INTERNAL_ERROR: "; break;
                case Severity::kERROR: std::cerr << "ERROR: "; break;
                case Severity::kWARNING: std::cerr << "WARNING: "; break;
                case Severity::kINFO: std::cerr << "INFO: "; break;
                default: std::cerr << "UNKNOWN: "; break;
            }
            std::cerr << msg << std::endl;
        }
} gLogger;

struct Profiler : public IProfiler
{
    typedef std::pair<std::string, float> Record;
    std::vector<Record> mProfile;

    virtual void reportLayerTime(const char* layerName, float ms)
    {
        auto record = std::find_if(mProfile.begin(), mProfile.end(),
                [&](const Record& r){ return r.first == layerName; });
        if (record == mProfile.end())
            mProfile.push_back(std::make_pair(layerName, ms));
        else
            record->second += ms;
    }

    void printLayerTimes()
    {
        float totalTime = 0;
        for (size_t i = 0; i < mProfile.size(); i++)
        {
            printf("%-40.40s %4.3fms\n",
                    mProfile[i].first.c_str(), mProfile[i].second / TIMING_ITERATIONS);
            totalTime += mProfile[i].second;
        }
        printf("Time over all layers: %4.3f\n", totalTime / TIMING_ITERATIONS);
    }

} gProfiler;

struct PPM
{
    std::string magic, fileName;
    int h, w, max;
    uint8_t buffer[INPUT_C * INPUT_H * INPUT_W];
};

struct BBOX
{
    float x1, y1, x2, y2;
    int cls;
};

struct OUTPUT_RESULT
{
    DimsCHW ConvDims;
    size_t ConvSize;
    float* ConvResult;

    DimsCHW BboxDims;
    size_t BboxSize;
    float* BboxResult;
};

std::string locateFile(const std::string& input)
{
    std::string file;
    std::vector<std::string> directories{"data/"};
    const int MAX_DEPTH{10};
    bool found{false};
    for (auto &dir : directories)
    {
        file = dir + input;
        for (int i = 0; i < MAX_DEPTH && !found; i++)
        {
            std::ifstream checkFile(file);
            found = checkFile.is_open();
            if (found) break;
            file = "../" + file;
        }
        if (found) break;
        file.clear();
    }

    assert(!file.empty() && "Could not find a file due to it not existing in the data directory.");
    return file;
}

// simple PPM (portable pixel map) reader
void readPPMFile(const std::string& filename, PPM& ppm)
{
    ppm.fileName = filename;
    std::ifstream infile(locateFile(filename), std::ifstream::binary);
    infile >> ppm.magic >> ppm.w >> ppm.h >> ppm.max;
    infile.seekg(1, infile.cur);
    infile.read(reinterpret_cast<char*>(ppm.buffer), ppm.w * ppm.h * 3);
}

void writePPMFileWithBBox(const std::string& filename, PPM& ppm, BBOX bbox)
{
    std::ofstream outfile("./" + filename, std::ofstream::binary);
    assert(!outfile.fail());
    outfile << "P6" << "\n" << ppm.w << " " << ppm.h << "\n" << ppm.max << "\n";
    auto round = [](float x)->int {return int(std::floor(x + 0.5f)); };
    for (int x = int(bbox.x1); x < int(bbox.x2); ++x)
    {
        // bbox top border
        ppm.buffer[(round(bbox.y1) * ppm.w + x) * 3] = 255;
        ppm.buffer[(round(bbox.y1) * ppm.w + x) * 3 + 1] = 0;
        ppm.buffer[(round(bbox.y1) * ppm.w + x) * 3 + 2] = 0;
        // bbox bottom border
        ppm.buffer[(round(bbox.y2) * ppm.w + x) * 3] = 255;
        ppm.buffer[(round(bbox.y2) * ppm.w + x) * 3 + 1] = 0;
        ppm.buffer[(round(bbox.y2) * ppm.w + x) * 3 + 2] = 0;
    }
    for (int y = int(bbox.y1); y < int(bbox.y2); ++y)
    {
        // bbox left border
        ppm.buffer[(y * ppm.w + round(bbox.x1)) * 3] = 255;
        ppm.buffer[(y * ppm.w + round(bbox.x1)) * 3 + 1] = 0;
        ppm.buffer[(y * ppm.w + round(bbox.x1)) * 3 + 2] = 0;
        // bbox right border
        ppm.buffer[(y * ppm.w + round(bbox.x2)) * 3] = 255;
        ppm.buffer[(y * ppm.w + round(bbox.x2)) * 3 + 1] = 0;
        ppm.buffer[(y * ppm.w + round(bbox.x2)) * 3 + 2] = 0;
    }
    outfile.write(reinterpret_cast<char*>(ppm.buffer), ppm.w * ppm.h * 3);
}

class ConvPlugin: public IPlugin
{
public:
    ConvPlugin(const Weights *weights, int nbWeights, int nbOutputChannels): mNbOutputChannels(nbOutputChannels)
    {
        assert(nbWeights == 2);
        mKernelWeights = copyToDevice(weights[0].values, weights[0].count);
        mBiasWeights = copyToDevice(weights[1].values, weights[1].count);
        assert(mBiasWeights.count == 0 || mBiasWeights.count == nbOutputChannels);
        // The input channels can be calculated with given weight information and output channels.
        mNbInputChannels = int(weights[0].count / nbOutputChannels / (kernel_w * kernel_h));
    }

    // create the plugin at runtime from a byte stream
    ConvPlugin(const void* data, size_t length)
    {
        const char* d = reinterpret_cast<const char*>(data), *a = d;
        mNbInputChannels = read<int>(d);
        mInputDims.d[0] = mNbInputChannels;
        mInputDims.d[1] = read<int>(d);
        mInputDims.d[2] = read<int>(d);
        mNbOutputChannels = read<int>(d);
        mOutputDims.d[0] = mNbOutputChannels;
        mOutputDims.d[1] = read<int>(d);
        mOutputDims.d[2] = read<int>(d);
        int biasCount = read<int>(d);

        mKernelWeights = deserializeToDevice(
                d, mNbInputChannels * mNbOutputChannels * kernel_w * kernel_h);
        mBiasWeights = deserializeToDevice(d, biasCount);
        assert(d == a + length);
    }

    ~ConvPlugin()
    {
    }

    int getNbOutputs() const override
    {
        // Return the number of outputs
        // TODO3
        // Hint:
        //     You can get the output number from the prototxt file.
        //
        /* return the output number */;
    }

    Dims getOutputDimensions(int index, const Dims* inputs, int nbInputDims) override
    {
        assert(index == 0 && nbInputDims == 1 && inputs[0].nbDims == 3);
        assert(mNbInputChannels == inputs->d[0]);

        // Calculate the output dimensions with given index and input dimensions.
        // TODO4
        // Hints:
        //     1. The dimension doesn't consider batch size, so generally it's in CHW bias.
        //
        //     2. For convolution, you can use the following formuals to calculate
        //        output feature map size,
        //
        //              h = (in[0].d[1] + padding_h * 2 - kernel_h)/stride_h + 1
        //              w = (in[0].d[2] + padding_w * 2 - kernel_w)/stride_w + 1
        //
        //     3. You can find the padding, kernel and stride information from those
        //        constant variables of this class.
        //
        return DimsCHW(/* the dimensions of output in CHW */);
    }

    void configure(const Dims* inputDims, int nbInputs, const Dims* outputDims, int nbOutputs, int maxBatchSize) override
    {
        mInputDims = *inputDims;
        mOutputDims = *outputDims;
    }

    int initialize() override
    {
        CHECK(cudnnCreate(&mCudnn));                            // initialize cudnn
        CHECK(cudnnCreateTensorDescriptor(&mSrcDescriptor));    // create cudnn tensor descriptors we need for bias addition
        CHECK(cudnnCreateTensorDescriptor(&mDstDescriptor));
        CHECK(cudnnCreateFilterDescriptor(&mFilterDescriptor));
        CHECK(cudnnCreateConvolutionDescriptor(&mConvDescriptor));
        CHECK(cudnnCreateTensorDescriptor(&mBiasDescriptor));

        return 0;
    }

    virtual void terminate() override
    {
        cudaFree(const_cast<void*>(mKernelWeights.values));
        cudaFree(const_cast<void*>(mBiasWeights.values));

        CHECK(cudnnDestroy(mCudnn));
    }

    virtual size_t getWorkspaceSize(int maxBatchSize) const override
    {
        // Return the required workspace size.
        // TensorRT could help to allocate the space for you (return the pointer
        // to you through the parameter of enqueue()).
        // Alternatively, you can do it by yourself and don't forget to free it in this
        // case after done.
        //
        // TODO5
        // Hint:
        //     You can use the predefined variable 'mCount' in this class.
        //
        /* return the workspace size */;
    }

    virtual int enqueue(int batchSize, const void*const * inputs, void** outputs, void* workspace, cudaStream_t stream) override
    {
        float kONE = 1.0f, kZERO = 0.0f;
        DimsNCHW srcStride = getStride(
                DimsCHW{mInputDims.d[0], mInputDims.d[1], mInputDims.d[2]});
        DimsNCHW dstStride = getStride(
                DimsCHW{mOutputDims.d[0], mOutputDims.d[1], mOutputDims.d[2]});

        cudnnSetStream(mCudnn, stream);

        CHECK(cudnnSetTensor4dDescriptorEx(
                    mSrcDescriptor,
                    CUDNN_DATA_FLOAT,
                    batchSize,
                    mInputDims.d[0], mInputDims.d[1], mInputDims.d[2],
                    srcStride.n(), srcStride.c(), srcStride.h(), srcStride.w()
                    ));

        CHECK(cudnnSetTensor4dDescriptorEx(
                    mDstDescriptor,
                    CUDNN_DATA_FLOAT,
                    batchSize,
                    mOutputDims.d[0], mOutputDims.d[1], mOutputDims.d[2],
                    dstStride.n(), dstStride.c(), dstStride.h(), dstStride.w()
                    ));

        CHECK(cudnnSetConvolution2dDescriptor(
                    mConvDescriptor,
                    padding_h, padding_w,
                    stride_h, stride_w, 1, 1,
                    CUDNN_CROSS_CORRELATION,
                    CUDNN_DATA_FLOAT));

        CHECK(cudnnSetFilter4dDescriptor(
                    mFilterDescriptor,
                    CUDNN_DATA_FLOAT,
                    CUDNN_TENSOR_NCHW,
                    mNbOutputChannels, mNbInputChannels,
                    kernel_w, kernel_h
                    ));

        // It invokes cuDNN API to do the real convolution with given parameters.
        // TODO6
        // Hints:
        //     1. prototype of cudnnConvolutionForward() can be found from cuDNN guide (http://docs.nvidia.com/deeplearning/sdk/cudnn-developer-guide/index.html#cudnnConvolutionForward).
        //     2. Fill these parameters with the arguments passed through.
        //
        CHECK(cudnnConvolutionForward(
                    mCudnn, &kONE, mSrcDescriptor,
                    /* Data pointer to the memory of input tensor */,
                    mFilterDescriptor,
                    mKernelWeights.values,
                    mConvDescriptor,
                    cudnnConvolutionFwdAlgo_t(0),
                    /* Data pointer to the work space */,
                    /* Size of the workspace in bytes */,
                    &kZERO,
                    mDstDescriptor,
                    /* Data pointer to the memory of output tensor */));

        if (mBiasWeights.count)
        {
            CHECK(cudnnSetTensor4dDescriptor(
                        mBiasDescriptor,
                        CUDNN_TENSOR_NCHW,
                        CUDNN_DATA_FLOAT,
                        1, mBiasWeights.count, 1, 1));

            CHECK(cudnnAddTensor(
                        mCudnn,
                        &kONE,
                        mBiasDescriptor,
                        mBiasWeights.values,
                        &kONE,
                        mDstDescriptor,
                        outputs[0]));
        }
        return 0;
    }

    virtual size_t getSerializationSize() override
    {
        // 7 integers (number of input channels, input h/w, number of output channels, output h/w, bias size), and then the weights:
        return sizeof(int)*7 + mKernelWeights.count*sizeof(float) + mBiasWeights.count*sizeof(float);
    }

    virtual void serialize(void* buffer) override
    {
        char* d = reinterpret_cast<char*>(buffer), *a = d;

        // Store the input dims
        write(d, mNbInputChannels);
        write(d, mInputDims.d[1]);
        write(d, mInputDims.d[2]);
        // Store the output dims
        write(d, mNbOutputChannels);
        write(d, mOutputDims.d[1]);
        write(d, mOutputDims.d[2]);
        write(d, (int)mBiasWeights.count);
        serializeFromDevice(d, mKernelWeights);
        serializeFromDevice(d, mBiasWeights);

        assert(d == a + getSerializationSize());
    }
private:
    template<typename T> void write(char*& buffer, const T& val)
    {
        *reinterpret_cast<T*>(buffer) = val;
        buffer += sizeof(T);
    }

    template<typename T> T read(const char*& buffer)
    {
        T val = *reinterpret_cast<const T*>(buffer);
        buffer += sizeof(T);
        return val;
    }

    Weights copyToDevice(const void* hostData, size_t count)
    {
        void* deviceData;
        CHECK(cudaMalloc(&deviceData, count * sizeof(float)));
        CHECK(cudaMemcpy(deviceData, hostData, count * sizeof(float), cudaMemcpyHostToDevice));
        return Weights{ DataType::kFLOAT, deviceData, int64_t(count) };
    }

    void serializeFromDevice(char*& hostBuffer, Weights deviceWeights)
    {
        cudaMemcpy(hostBuffer, deviceWeights.values, deviceWeights.count * sizeof(float), cudaMemcpyDeviceToHost);
        hostBuffer += deviceWeights.count * sizeof(float);
    }

    Weights deserializeToDevice(const char*& hostBuffer, size_t count)
    {
        Weights w = copyToDevice(hostBuffer, count);
        hostBuffer += count * sizeof(float);
        return w;
    }

    DimsNCHW getStride(DimsCHW d)
    {
        int w, h, c, n;
        w = 1, h = d.w()*w, c = d.h()*h, n = d.c()*c;
        return DimsNCHW{ n, c, h, w };
    }

    int mNbOutputChannels, mNbInputChannels;
    Dims mInputDims;
    Dims mOutputDims;
    void * mWorkSpace;
    const unsigned int mCount = 10 << 20; // 10M work space size in bytes

    // there's no way to pass parameters through from the model definition, so we have to define it here explicitly
    const int kernel_w = 7; // kernel size
    const int kernel_h = 7;
    const int stride_w = 2; // stride
    const int stride_h = 2;
    const int padding_w = 3; // padding
    const int padding_h = 3;

    cudnnHandle_t mCudnn;
    Weights mKernelWeights, mBiasWeights;
    cudnnTensorDescriptor_t mSrcDescriptor, mDstDescriptor, mBiasDescriptor;
    cudnnFilterDescriptor_t mFilterDescriptor;
    cudnnConvolutionDescriptor_t mConvDescriptor;
};

// Assisant class used to correlate TensorRT with IPlugin layer.
class PluginFactory : public nvinfer1::IPluginFactory, public nvcaffeparser1::IPluginFactory
{
    public:
        bool isPlugin(const char* name) override
        {
            // Whatever layer TensorRT visits, it always checks whether it is
            // a plugin layer. If true, it will invoke your own layer implementation. 
            //
            // TODO1
            // Hints:
            //     1. You can use strcmp to compare two name strings.
            //     2. The target layer name is 'Layer1'.
            //
            /* Whether it's a plugin layer */;
        }

        // Constructor used in build phase
        virtual nvinfer1::IPlugin* createPlugin(const char* layerName, const nvinfer1::Weights* weights, int nbWeights) override
        {
            // TensorRT have no idea regrading what kind of parameters your target layer
            // needs, so it's impossible to define an unified interface to pass through
            // the unpredictable parameter to you. Instead it just returns the weight
            // information (appear to be only valid for weighted layer).
            // You can explictly hack the needed parameter as a constant when you use it,
            // such as for output feature maps, stride, pad and ect.
            //
            // TODO2
            // Hint:
            //      You can get the channel information of your target layer from
            //      prototxt file
            //
            static const int NB_OUTPUT_CHANNELS = /* number of output channels */;
            assert(isPlugin(layerName) && nbWeights == 2 && weights[0].type == DataType::kFLOAT && weights[1].type == DataType::kFLOAT);
            assert(mPlugin.get() == nullptr);
            mPlugin = std::unique_ptr<ConvPlugin>(new ConvPlugin(weights, nbWeights, NB_OUTPUT_CHANNELS));
            return mPlugin.get();
        }

        // Constructor used in execution phase
        // deserialization plugin implementation
        IPlugin* createPlugin(const char* layerName, const void* serialData, size_t serialLength) override
        {
            assert(isPlugin(layerName));
            assert(mPlugin.get() == nullptr);
            mPlugin = std::unique_ptr<ConvPlugin>(new ConvPlugin(serialData, serialLength));
            return mPlugin.get();
        }

        // the application has to destroy the plugin when it knows it's safe to do so
        void destroyPlugin()
        {
            mPlugin.release();
        }

        std::unique_ptr<ConvPlugin> mPlugin{ nullptr };
};

void caffeToTRTModel(const char* deployFile,
        const char* modelFile,
        const std::vector<std::string>& outputs,
        unsigned int maxBatchSize,
        nvcaffeparser1::IPluginFactory* pluginFactory,
        IHostMemory *&trtModelStream)
{
    // Create API root class - must span the lifetime of the engine usage
    IBuilder* builder = createInferBuilder(gLogger);
    INetworkDefinition* network = builder->createNetwork();

    // Parse the caffe model to populate the network
    // Create a 16-bit model if it's natively supported
    bool useFp16 = builder->platformHasFastFp16();
    ICaffeParser* parser = createCaffeParser();
    parser->setPluginFactory(pluginFactory);
    const IBlobNameToTensor *blobNameToTensor = parser->parse(
            deployFile,
            modelFile,
            *network,
            useFp16 ? DataType::kHALF: DataType::kFLOAT);
    assert(blobNameToTensor != nullptr);

    // The caffe file has no notion of outputs, so we need to explicitly specify
    // which tensors the engine should generate
    for (auto& s : outputs)
        network->markOutput(*blobNameToTensor->find(s.c_str()));

    // Configure the batch size and maxWorkspaceSize
    builder->setMaxBatchSize(maxBatchSize);
    builder->setMaxWorkspaceSize(1 << 30);
    // Set up the network for paired-fp16 format if available
    if(useFp16)
        builder->setHalf2Mode(true);

    // Build the engine
    ICudaEngine* engine = builder->buildCudaEngine(*network);
    assert(engine);

    // We don't need the network any more, and we can destroy the parser
    network->destroy();
    parser->destroy();

    // Serialize the engine, then close everything down
    trtModelStream = engine->serialize();
    engine->destroy();
    builder->destroy();
    shutdownProtobufLibrary();
}

void fillImageData(float* imageData)
{
    std::vector<PPM> ppms(BATCH_SIZE);

    assert(ppms.size() <= IMAGE_LIST.size());

    for (int i = 0; i < BATCH_SIZE; ++i)
        readPPMFile(IMAGE_LIST[i], ppms[i]);

    for (int i = 0, volImg = INPUT_C * INPUT_H * INPUT_W; i < BATCH_SIZE; ++i)
    {
        for (int c = 0; c < INPUT_C; ++c)
        {
            // the color image to input should be in BGR order
            for (unsigned j = 0, volChl = INPUT_H * INPUT_W; j < volChl; ++j)
                imageData[i * volImg + c * volChl + j] =
                    float(ppms[i].buffer[j * INPUT_C + 2 - c]) * SCALER;
        }
    }
}

void doInference(
        IExecutionContext& context,
        int batchSize,
        float* input,
        struct OUTPUT_RESULT& output)
{
    const ICudaEngine& engine = context.getEngine();

    // Bindings means the input and output buffer pointers that
    // we pass to the engine.
    // The engine requires exactly ICudaEngine::getNbBindings() of these,
    // but in this case we know that there is exactly one input and two outputs.
    assert(engine.getNbBindings() == 3);
    void* buffers[3];

    // In order to bind the buffers, we need to know the names of the input
    // and output tensors.
    // Note that indices are guaranteed to be less than ICudaEngine::getNbBindings()
    int inputIndex = engine.getBindingIndex(INPUT_BLOB_NAME);
    DimsCHW inputDims =
        static_cast<DimsCHW&&>(engine.getBindingDimensions(inputIndex));
    size_t inputSize =
        batchSize * inputDims.c() * inputDims.h() * inputDims.w() * sizeof(float);

    // Output Conv Dimension and size
    int outputConvIndex= engine.getBindingIndex(OUTPUT_BLOB_CONV_NAME);
    output.ConvDims =
        static_cast<DimsCHW&&>(engine.getBindingDimensions(outputConvIndex));
    output.ConvSize =
        batchSize * output.ConvDims.c() * output.ConvDims.h() * output.ConvDims.w() * sizeof(float);

    // Output Bbox Dimension and size
    int outputBboxIndex = engine.getBindingIndex(OUTPUT_BLOB_BBOX_NAME);
    output.BboxDims =
        static_cast<DimsCHW&&>(engine.getBindingDimensions(outputBboxIndex));
    output.BboxSize =
        batchSize * output.BboxDims.c() * output.BboxDims.h() * output.BboxDims.w() * sizeof(float);

    // Allocate GPU buffers
    CHECK(cudaMalloc(&buffers[inputIndex], inputSize));
    CHECK(cudaMalloc(&buffers[outputConvIndex], output.ConvSize));
    CHECK(cudaMalloc(&buffers[outputBboxIndex], output.BboxSize));

    // Copy the input buffer
    CHECK(cudaMemcpy(buffers[inputIndex], input, inputSize,
                cudaMemcpyHostToDevice));

    // Do real inference on the input data
    for (int i = 0; i < TIMING_ITERATIONS;i++)
        context.execute(batchSize, buffers);

    // Allocate CPU buffers to store the result
    output.ConvResult = new float[output.ConvSize];
    output.BboxResult = new float[output.BboxSize];

    CHECK(cudaMemcpy(output.ConvResult, buffers[outputConvIndex], output.ConvSize,
                cudaMemcpyDeviceToHost));
    CHECK(cudaMemcpy(output.BboxResult, buffers[outputBboxIndex], output.BboxSize,
                cudaMemcpyDeviceToHost));

    // Release the GPU buffers
    // CPU Buffer will be released after bbox parsing done
    CHECK(cudaFree(buffers[inputIndex]));
    CHECK(cudaFree(buffers[outputConvIndex]));
    CHECK(cudaFree(buffers[outputBboxIndex]));
}

void parseResult(struct OUTPUT_RESULT& output)
{
    DimsCHW convDims = output.ConvDims;
    DimsCHW bboxDims = output.BboxDims;

    assert(CLASS_NUM == convDims.c());

    int grid_x_ = convDims.w();
    int grid_y_ = convDims.h();
    int gridsize_ = grid_x_ * grid_y_;
    int target_shape[2] = {grid_x_, grid_y_};
    float bbox_norm[2] = {640.0, 368.0};
    float gc_centers_0[target_shape[0]];
    float gc_centers_1[target_shape[1]];
    for (int i = 0; i < target_shape[0]; i++)
    {
        gc_centers_0[i] = (float)(i * 16 + 0.5);
        gc_centers_0[i] /= (float)bbox_norm[0];

    }
    for (int i = 0; i < target_shape[1]; i++)
    {
        gc_centers_1[i] = (float)(i * 16 + 0.5);
        gc_centers_1[i] /= (float)bbox_norm[1];
    }

    std::ofstream bbox_result;
    bbox_result.open("bbox_result.txt", std::ofstream::out | std::ofstream::trunc);

    for (int i = 0; i < BATCH_SIZE; i++)
    {
        std::vector<cv::Rect> *rectListClass;
        rectListClass = new std::vector<cv::Rect>[CLASS_NUM];
        BBOX bbox[BBOX_MAX_NUM];
        int bbox_num = 0;
        float* convPtr = output.ConvResult + i * convDims.c() * convDims.h() * convDims.w();
        float* bboxPtr = output.BboxResult + i * bboxDims.c() * bboxDims.h() * bboxDims.w();
        for (int c = 0; c < CLASS_NUM; c++)
        {
            const float *output_x1 = bboxPtr + c * (bboxDims.c()/CLASS_NUM) * bboxDims.h() * bboxDims.w();
            const float *output_y1 = output_x1 + bboxDims.h() * bboxDims.w();
            const float *output_x2 = output_y1 + bboxDims.h() * bboxDims.w();
            const float *output_y2 = output_x2 + bboxDims.h() * bboxDims.w();
            for (int h = 0; h < grid_y_; h++)
            {
                for (int w = 0; w < grid_x_; w++)
                {
                    int j = w + h * grid_x_;
                    if (convPtr[c * gridsize_ + j] >= CLASS_THRESHOLD[c])
                    {
                        float rectx1_f, recty1_f, rectx2_f, recty2_f;
                        int rectx1, recty1, rectx2, recty2;

                        rectx1_f = output_x1[w + h * grid_x_] - gc_centers_0[w];
                        recty1_f = output_y1[w + h * grid_x_] - gc_centers_1[h];
                        rectx2_f = output_x2[w + h * grid_x_] + gc_centers_0[w];
                        recty2_f = output_y2[w + h * grid_x_] + gc_centers_1[h];

                        rectx1_f *= (float)(-bbox_norm[0]);
                        recty1_f *= (float)(-bbox_norm[1]);
                        rectx2_f *= (float)(bbox_norm[0]);
                        recty2_f *= (float)(bbox_norm[1]);

                        rectx1 = (int)rectx1_f;
                        recty1 = (int)recty1_f;
                        rectx2 = (int)rectx2_f;
                        recty2 = (int)recty2_f;

                        rectx1 = rectx1 < 0 ? 0 :
                            (rectx1 >= INPUT_W ? (INPUT_W - 1) : rectx1);
                        rectx2 = rectx2 < 0 ? 0 :
                            (rectx2 >= INPUT_W ? (INPUT_W - 1) : rectx2);
                        recty1 = recty1 < 0 ? 0 :
                            (recty1 >= INPUT_H ? (INPUT_H - 1) : recty1);
                        recty2 = recty2 < 0 ? 0 :
                            (recty2 >= INPUT_H ? (INPUT_H - 1) : recty2);

                        rectListClass[c].push_back(cv::Rect(rectx1, recty1, rectx2-rectx1, recty2-recty1));
                    }
                }
            }
            cv::groupRectangles(rectListClass[c], 1, 0.2);

            std::vector<cv::Rect>& rectList = rectListClass[c];
            for (int iRect = 0; iRect < (int)rectList.size(); ++iRect)
            {
                cv::Rect& r = rectList[iRect];
                bbox[bbox_num].x1 = (float)r.x;
                bbox[bbox_num].y1= (float)r.y;
                bbox[bbox_num].x2 = bbox[bbox_num].x1 + (float)r.width;
                bbox[bbox_num].y2 = bbox[bbox_num].y1 + (float)r.height;
                bbox[bbox_num].cls = c;
                bbox_num++;
           }
        }

        // If there is bbox detected, we draw it on the original image
        if (bbox_num)
        {
            PPM ppm;
            readPPMFile(IMAGE_LIST[i], ppm);
            std::string storeName = IMAGE_LIST[i].substr(0, 2) + "_bboxed.ppm";

            for (int k = 0; k < bbox_num; k++)
            {
                std::cout << "Drawing bbox (" << CLASS_NAME[bbox[k].cls] <<
                    "," << bbox[k].x1 <<
                    "," << bbox[k].y1 <<
                    "," << bbox[k].x2 <<
                    "," << bbox[k].y2 <<
                    ") " << "on the " << IMAGE_LIST[i] << std::endl;
                writePPMFileWithBBox(storeName, ppm, bbox[k]);
                if (bbox_result.is_open())
                {
                    bbox_result <<
                        i << "," <<
                        bbox[k].x1 << "," <<
                        bbox[k].y1 << "," <<
                        bbox[k].x2 << "," <<
                        bbox[k].y2 << "\n";
                }
            }
        }
        delete [] rectListClass;
    }
    bbox_result.close();
}

int main(int argc, char** argv)
{
    IHostMemory *trtModelStream{nullptr};

    ////////////////////////////////////////////////////////////////////
    // <0> Instantiate a pluginFactory
    ////////////////////////////////////////////////////////////////////
    PluginFactory pluginFactory;

    ////////////////////////////////////////////////////////////////////
    // <1> Parse the caffe model into TensorRT recognized stream
    ////////////////////////////////////////////////////////////////////
    //
    // In build phase, TensorRT needs to invoke the implemented APIs to handle
    // the processing steps for IPlugin layer.
    //
    // TODO7
    caffeToTRTModel(
            DEPLOY_FILE,
            MODEL_FILE,
            std::vector<std::string>{OUTPUT_BLOB_CONV_NAME, OUTPUT_BLOB_BBOX_NAME},
            BATCH_SIZE,
            /* Pointer to pluginFactory */,
            trtModelStream);

    pluginFactory.destroyPlugin();

    ////////////////////////////////////////////////////////////////////
    // <2> Deserialize the model stream and create an execution context
    ////////////////////////////////////////////////////////////////////
    //
    // In execution phase, TensorRT needs to invoke the deserialization API
    // to reconstruct the network graph, so we also need to reference pluginFactory.
    //
    // TODO8
    IRuntime* runtime = createInferRuntime(gLogger);
    ICudaEngine* engine = runtime->deserializeCudaEngine(
            trtModelStream->data(),
            trtModelStream->size(),
            /* Pointer to pluginFactory */);
    IExecutionContext *context = engine->createExecutionContext();
    //context->setProfiler(&gProfiler);

    // Allocate resource for input image and output result
    float* inputData = new float[BATCH_SIZE * INPUT_C * INPUT_H * INPUT_W];
    struct OUTPUT_RESULT outputData;

    ////////////////////////////////////////////////////////////////////
    // <3> Prepare image data
    ////////////////////////////////////////////////////////////////////
    fillImageData(inputData);

    ////////////////////////////////////////////////////////////////////
    // <4> Run real inference on the image
    ////////////////////////////////////////////////////////////////////
    doInference(*context, BATCH_SIZE, inputData, outputData);

    ////////////////////////////////////////////////////////////////////
    // <5> Parse the output result
    ////////////////////////////////////////////////////////////////////
    parseResult(outputData);

    // Destroy the runtime
    context->destroy();
    engine->destroy();
    runtime->destroy();
    pluginFactory.destroyPlugin();

    // Release the input and output CPU buffers
    delete[] inputData;
    delete[] outputData.ConvResult;
    delete[] outputData.BboxResult;

    // Print the execution time
    //gProfiler.printLayerTimes();

    return 0;
}
