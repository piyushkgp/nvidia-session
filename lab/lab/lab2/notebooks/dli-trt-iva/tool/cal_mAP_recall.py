import argparse

def compute_IoU(bbox1, bbox2):
    '''
    Calculate the Intersection over Union (IoU) of two bounding boxes
    The elements of bbox mean,

       (bbox[0], bbox[1]) --------------
              |                        |
              |                        |
              |                        |
              |                        |
              |                        |
              |-------------------(bbox[2], bbox[3])

    '''
    x_left   = max(bbox1[0], bbox2[0])
    y_top    = max(bbox1[1], bbox2[1])
    x_right  = min(bbox1[2], bbox2[2])
    y_bottom = min(bbox1[3], bbox2[3])

    overlay_area = (x_right - x_left) * (y_bottom - y_top)

    union_area = (bbox1[2] - bbox1[0]) * (bbox1[3] - bbox1[1]) +\
                 (bbox2[2] - bbox2[0]) * (bbox2[3] - bbox2[1]) - overlay_area

    IoU = float(overlay_area)/union_area
    if IoU > 1 or IoU < 0:
        return 0
    else:
        return IoU

def compute_mAP_Recall(results):
    '''
        Compute the mean average and recall rate of given bbox information
            mean Average Precision = TP/(TP + FP)
            Recall Rate = TP/(TP + FN)
    '''

    # Set max bbox num of one image as 32
    # each frame has 32 bboxes at most and 4 coordinates
    frame_num, max_num, coord = results.bs, 32, 4

    # Read out bbox information from the file with predictions
    # bb_pd[2][32][4]
    #       The first dimension represents the number of frames
    #       The second dimension represents the number of bbox
    #       The third dimension represents the coordinates
    # bb_pd_num[2] represents the bbox number
    bb_pd = [[[0 for x in range(coord)] for y in range(max_num)] for f in range(frame_num)]
    bb_pd_num = [0 for x in range(frame_num)]
    with open(results.pd_path, 'r') as f:
        print "Predicted BBox: "
        bb_num = 0
        last_bb_str = "a"
        for line in f.readlines():
            bb_str = line.strip().split(',')
            if bb_str[0] != last_bb_str[0]:
                last_bb_str = bb_str
                bb_num = 0
            bb_pd[int(bb_str[0])][bb_num] = list(map(int, bb_str[1:]))
            print "\t" + "Frame " + bb_str[0] + " --> " + \
                str(bb_pd[int(bb_str[0])][bb_num])
            bb_num += 1
            bb_pd_num[int(bb_str[0])] = bb_num

    # Read out bbox information from the file with ground truth
    bb_gt = [[[0 for x in range(coord)] for y in range(max_num)] for f in range(frame_num)]
    bb_gt_num = [0 for x in range(frame_num)]
    with open(results.gt_path, 'r') as f:
        print "Ground Truth: "
        bb_num = 0
        last_bb_str = "a"
        for line in f.readlines():
            bb_str = line.strip().split(',')
            if bb_str[0] != last_bb_str[0]:
                last_bb_str = bb_str
                bb_num = 0
            bb_gt[int(bb_str[0])][bb_num] = list(map(int, bb_str[1:]))
            print "\t" + "Frame " + bb_str[0] + " --> " + \
                str(bb_gt[int(bb_str[0])][bb_num])
            bb_num += 1
            bb_gt_num[int(bb_str[0])] = bb_num

    # Set IoU threshold to 0.75.
    # To simplify the process, we treat all three classes as an entire part.
    # Bbox might be appearing in different order from ground truth file
    # and prediction file, so we iterate all of them and figure out the max
    # one as the final result.
    # This way won't be working when two bboxs from different classes has big IoU.
    AP = [0 for x in range(frame_num)]
    RR = [0 for x in range(frame_num)]
    for f in range(frame_num):
        true_p  = 0
        false_p = 0
        false_n = 0
        for i in range(bb_pd_num[f]):
            IoU = 0
            for j in range(bb_gt_num[f]):
                IoU = max(IoU, compute_IoU(bb_pd[f][i], bb_gt[f][j]))
            if IoU > 0.75:
                true_p += 1
            else:
                false_p += 1

            assert bb_gt_num[f] >= bb_pd_num[f]
            false_n += bb_gt_num[f] - bb_pd_num[f]

        AP[f] = true_p / float(true_p + false_p)
        RR[f] = true_p / float(true_p + false_n)

    print "mean Average Precision = " + str(sum(AP)/frame_num)
    print "Recall Rate            = " + str(sum(RR)/frame_num)

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description=\
            'Compute mAP and recall rate of detection network')

    parser.add_argument('-bs', action="store", dest="bs",
            help="Batch size", type=int, required=True)
    parser.add_argument("-gt", action='store', dest='gt_path',
            help='Store the path of ground truth bbox', required=True)
    parser.add_argument("-pd", action='store', dest='pd_path',
            help='Store the path of predicted bbox', required=True)

    results = parser.parse_args()

    compute_mAP_Recall(results)
